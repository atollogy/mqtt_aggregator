const fse = require('fs-extra');
var zlib = require("zlib");
var debug = require('debug')('mqtt_aggregator');
const moment = require("moment");

var API = require("./api.js");

const ARCHIVE_VERSION = 1;

// data cache
var cache = {}; //<slotkey>:{updatedTime, gateways:{<gatewayId>: {<beaconId>: [<raw advertisement>]}}}
var backlog = [];
var recordsInProgress = [];

function _clear(){
    Object.keys(cache).forEach(function(key){
        delete cache[key];
    });
    backlog.splice(0,backlog.length);
};

function makeSlot(unixTimestamp){
    return moment.unix(unixTimestamp).startOf("minute").unix();
}

function store(advertisement){
    if (!advertisement.namespace || !advertisement.instance || !advertisement.gateway_id) {
        console.log(`rejecting beacon because of missing identifiers`);
        return;
    }

    if (advertisement.tx_power_1m === undefined || advertisement.tx_power_1m > -21 || advertisement.tx_power_1m < -141) {
        console.log(`rejecting beacon because of invalid tx_power_1m: ${JSON.stringify(advertisement)}`);
        return;
    }

    if (advertisement.rssi === undefined || advertisement.rssi > 20) {
        console.log(`rejecting beacon because of invalid rssi: ${JSON.stringify(advertisement)}`);
        return;
    }

    if (!advertisement['provenance']) {
        //console.log(`rejecting beacon because of missing provenance: ${JSON.stringify(advertisement)}`)
        return;
    }

    var slot = makeSlot(advertisement.lastSeen);
    var gatewayId = advertisement.gateway_id;
    var beaconId = advertisement.namespace + advertisement.instance;

    if (!cache.hasOwnProperty(slot)) {
        cache[slot] = {updatedTime: new Date(), gateways:{}};
    } else {
        cache[slot].updatedTime = new Date();
    }

    if (!cache[slot].gateways.hasOwnProperty(gatewayId)) {
        cache[slot].gateways[gatewayId] = {};
    }

    if (cache[slot].gateways[gatewayId].hasOwnProperty(beaconId)) {
        cache[slot].gateways[gatewayId][beaconId].push(advertisement);
    } else {
        cache[slot].gateways[gatewayId][beaconId] = [advertisement];
    }
}

function remove(slot){
    var result = cache[slot];
    delete cache[slot];
    return result;
}

var archiveFileName = "archive_1";

// Save state out to disk. Must be synchronous
function archive(location){
    var fileName = location + "/" + archiveFileName + ".json.gz";
    if(Object.keys(cache).length || backlog.length || recordsInProgress.length){
        console.log("Archiving Aggregator state for shutdown.");
        fse.ensureDirSync(location);

        // these keys don't need to be written out, since we construct them on restore
        // and we're about to exit, so we can just modify the structures in place
        for(key in cache){
            delete cache[key].updatedTime;
        }
        backlog.forEach(function(entry){
            delete entry.backlog;
        });

        var outputJSON = JSON.stringify({version: ARCHIVE_VERSION, cache:cache, backlog:backlog.concat(recordsInProgress)});
        var zippedOutput = zlib.gzipSync(outputJSON);
        fse.writeFileSync(fileName, zippedOutput);
    } 
}

function restore(location){
    var rawFileName = location + "/" + archiveFileName + ".json";
    var zipFileName = rawFileName + ".gz";

    var hasRaw = fse.existsSync(rawFileName);
    var hasZip = fse.existsSync(zipFileName);

    if(hasRaw || hasZip){
        console.log("Recovering Aggregator state.");
        // read the archive file
        try{
            var rawData
            if(hasZip){
                rawData = zlib.unzipSync(fse.readFileSync(zipFileName));
            }else{
                rawData = fse.readFileSync(rawFileName);
            }
            var recoveredData = JSON.parse(rawData);
            
            if (recoveredData.version == ARCHIVE_VERSION){
                // copy the old cache into the cache
                var now = new Date();
                var beaconSlotCount = 0;
                var readingCount = 0;
                Object.keys(recoveredData.cache).forEach(function(cacheKey){
                    cache[cacheKey] = recoveredData.cache[cacheKey];
                    cache[cacheKey].updatedTime = now;
                    //calculate stats
                    if(cache[cacheKey].gateways){
                        for (gatewayId in cache[cacheKey].gateways){
                            for(beaconId in cache[cacheKey].gateways[gatewayId]){
                                beaconSlotCount += 1;
                                readingCount += cache[cacheKey].gateways[gatewayId][beaconId].length;
                            }
                        }
                    }
                });
                // copy the old backlog into the backlog
                Array.prototype.push.apply(backlog, recoveredData.backlog.map(function(record){
                    record.backlog = true;
                    return record;
                }));

                console.log("Recovered records from backlog: " + recoveredData.backlog.length);
                console.log("Recovered aggregator state: Beacon Slots: " + beaconSlotCount + ", readings: " + readingCount);
            } else {
                console.log("Archive version changed, skipping record recovery");
            }
        } catch(e){
            _clear(); //cache may be in an incomplete state, so ignore it
            console.log("Archive Recovery Failed");
            console.error(e);
        }
        
        //remove the archive
        if (hasRaw) {
            fse.removeSync(rawFileName);
        }
        if (hasZip) {
            fse.removeSync(zipFileName);
        }

    }
}

// Shim for 6.x node versions that don't have Object.Values
function getValues(obj){
    return Object.keys(obj).map(function(key){return obj[key]; });
}

function aggregateCompletedSlots(time){
    var timeInSeconds = Math.floor(time.getTime()/1000.0);

    var aggregatedRecords = [];
    Object.keys(cache).filter(function(slotKey){
        return  timeInSeconds - slotKey > 65 //the key represents times from more than 65 seconds ago
            && time.getTime() - cache[slotKey].updatedTime.getTime() > 60*1000; //the key has not been updated for one minute
    }).forEach(function(slotKey){
        debug("aggregating records for slot " + slotKey);

        var slotData = remove(slotKey);

        getValues(slotData.gateways).forEach(function(beaconData){
            getValues(beaconData).forEach(function(advertisementList){
                try {
                    aggregatedRecords.push(aggregateGWData(advertisementList));
                } catch (e){
                    console.error(e);
                }
            });
        });
    });

    return aggregatedRecords;
}

// aggregate

function calculateDistance(rssi, tx_power_1m){
    return Math.pow(10, (tx_power_1m - rssi) / 20.0)
}

function stdDev(arr){
    var avg = arr.reduce( (a,b) => a+b ) / arr.length;
    return Math.sqrt(arr.map( x => avg - x).map( x => x * x).reduce( (a,b) => a+b ) / arr.length);
}

function aggregateGWData(data) {
    if (!data || data.length == 0) {
        return null;
    }

    var agg = {
        beacon_id: data[0].instance,
        slot: makeSlot(data[0].lastSeen), 
        "slot_raw_data": []
    };

    ["gateway_id", "namespace", "mac", "provenance", "beacon_type", "beacon_subtype", "nodename"].forEach((key)=>{
        agg[key] = data[0][key];
    });

    var rssi_sample = [];
    var tx_power_sample = [];
    var distances = [];
    var bestReading = undefined;

    data.forEach(function(item, index){
        if ( typeof item.rssi == 'number' && typeof item.tx_power_1m == 'number') {
            var distance = calculateDistance(item.rssi, item.tx_power_1m);
            var rawData = {t: item.lastSeen , s: item.rssi, p: item.tx_power_1m, d: distance};
            agg.slot_raw_data.push(rawData);
            rssi_sample.push(item.rssi);
            tx_power_sample.push(item.tx_power_1m);
            distances.push(distance);

            if (!bestReading || distance < bestReading.d){
                bestReading = rawData;
            }
        } else {
            console.log(`bad record: ${JSON.stringify(item)}`)
            return;
        }
    });

    // average rssi
    agg.avg_rssi = (rssi_sample.length) ? rssi_sample.reduce(function(a,b){ return a+b; }, 0) / rssi_sample.length : null ;
    agg.tx_power_1m = (rssi_sample.length) ? tx_power_sample.reduce(function(a,b){ return a+b; }, 0) / rssi_sample.length : null ;

    if (typeof agg.avg_rssi != 'number') {
        console.dir(agg);
        throw 'Average rssi is missing.'
    }

    agg.sd_rssi = stdDev(rssi_sample);

    var distances = agg.slot_raw_data.map( x => x.d );
    agg.sd_distance = stdDev(distances);

    // average distance
    agg.avg_distance = calculateDistance(agg.avg_rssi, agg.tx_power_1m);

    if (agg.avg_distance <= 0) {
        console.dir(agg);
        throw `Average distance less than zero: ${agg.avg_distance}`
    } else if (agg.avg_distance < 1.33) {
        agg.distance_proximity = 'at_workstation';
    } else if (agg.avg_distance < 4.0) {
        agg.distance_proximity = 'in_work_area';
    } else if (agg.avg_distance < 12.0) {
        agg.distance_proximity = 'in_work_zone';
    } else if (agg.avg_distance <= 72.0) {
        agg.distance_proximity = 'at_facility';
    } else if (typeof agg.avg_distance === 'undefined' || agg.avg_distance === null) {
        console.dir(agg);
        throw 'Average distance is null.'
    } else {
        agg.distance_proximity = 'indeterminate';
    }

    agg.best_rssi = bestReading && bestReading.s;
    agg.best_tx_power_1m = bestReading && bestReading.p;
    agg.best_distance = bestReading && bestReading.d;
    agg.best_time = bestReading && bestReading.t

    agg.reading_count = agg.slot_raw_data.length;

    return agg;
}

var uploadRunning = false;

function upload(now){
    if (uploadRunning){
        console.log("MQTT Aggregator upload already running");
        return Promise.resolve(null);
    }
    uploadRunning = true;
    if(!now){
        now = new Date();
    }

    var newRecords = aggregateCompletedSlots(now);

    var recordsToUpload = newRecords.concat(backlog);
    recordsInProgress = newRecords;

    console.log("MQTT Aggregator upload started: " +recordsToUpload.length + " records");
    
    return API.beaconReadings(recordsToUpload).then(function(result){
        if (backlog.length){
            //remove backlog files
            backlog.splice(0,backlog.length);
        }
        console.log("MQTT Aggregator uploaded " +recordsToUpload.length + " records");
    }).catch(function(err){
        //store newRecords to backlog
        console.error(err);
        Array.prototype.push.apply(backlog, newRecords.map(function(record){
            record.backlog = true;
            return record;
        }));
        console.log("MQTT Aggregator upload failed, total backlog is now: " + backlog.length);
    }).then(function(){
        recordsInProgress = [];
        uploadRunning = false;
    });
}

module.exports = {
    _ARCHIVE_VERSION : ARCHIVE_VERSION,
    _cache: cache,
    _backlog: backlog,
    _clear: _clear,
    _makeSlot: makeSlot,
    aggregateGWData:aggregateGWData,
    store: store,
    remove: remove,
    archive: archive,
    restore: restore,
    aggregateCompletedSlots: aggregateCompletedSlots,
    upload: upload
}
