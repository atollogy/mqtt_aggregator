var fs = require("fs-extra");

var bcm = {
    gateway_id: '000000000000',
    scode:0,
    svcs:{0:{data_dir:"./work", sensorCfg:{version:"xxxxxx",motion:{video0:{snapshot_interval: 60}}}}},
    endpoints:{atlapi:{}},
    nodename:"nodename",
    aws_creds:{akid:'', sak:''},
    aws_region:''
}

if(fs.existsSync('/etc/kvmanager/bcm.json')){
    bcm = fs.readJsonSync('/etc/kvmanager/bcm.json');
}

var datadir = bcm.svcs[bcm.scode].data_dir;
var motionConfigVersion = "default";
var motionSnapshotInterval = undefined;
if (bcm.svcs[bcm.scode].sensorCfg &&
        bcm.svcs[bcm.scode].sensorCfg.version !== null &&
        bcm.svcs[bcm.scode].sensorCfg.version !== undefined){
    motionConfigVersion = bcm.svcs[bcm.scode].sensorCfg.version;
    if(bcm.svcs[bcm.scode].sensorCfg.motion && bcm.svcs[bcm.scode].sensorCfg.motion.video0) {
        motionSnapshotInterval = bcm.svcs[bcm.scode].sensorCfg.motion.video0.snapshot_interval * 1000;
    }
}

var gateway_id = bcm.gateway_id && /^[0-9A-Fa-f]{12}$/.test(bcm.gateway_id) ? bcm.gateway_id.toLowerCase() : undefined;
if (!gateway_id){
    console.warn("Missing gateway id at aggregator startup");
}

module.exports = {
    mqttServer: bcm.svcs[bcm.scode].mqtt_server,

    archiveLocation: `${datadir}/archive`,

    nodename: bcm.nodename,
    gateway_id: gateway_id,
    customerId: bcm.cgr,
    apiProtocol: bcm.endpoints.atlapi.protocol || "https",
    apiHost: bcm.endpoints.atlapi.host,
    apiVersion: bcm.endpoints.atlapi.api_version,

    image_bundle_backlog_dir: `${datadir}/image_bundle_backlog`,

    motionConfigVersion: motionConfigVersion,
    motionSnapshotInterval: motionSnapshotInterval,

    aws: {
        region: bcm.aws_region,
        akid: bcm.aws_creds.akid,
        sak: bcm.aws_creds.sak
    }
}
