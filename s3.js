// Load the SDK for JavaScript
const AWS = require('aws-sdk');
const fs = require('fs');
const config = require("./configuration.js");

// AWS Configuration
AWS.config.update({
    accessKeyId: config.aws.akid,
    secretAccessKey: config.aws.sak,
    region: config.aws.region
});

var s3 = new AWS.S3();

module.exports = {
    savetoS3: function (bucket, fileName, image) {
        var bodystream = fs.createReadStream(image);
        var params = {
            Bucket: bucket,
            Key: fileName,
            Body: bodystream,
        };
        return s3.putObject(params).promise();
    }
}