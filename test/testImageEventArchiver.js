import test from 'ava';
import sinon from 'sinon';
import fse from 'fs-extra';
import tar from 'tar-stream';
import {Readable} from 'stream';

import archiver from '../imageEventArchiver.js';

const testFileLocation = "./tmp/imageEventArchiver";
var fileId = 0;

var testImageData = fse.readFileSync("./testData/20170814112430-snapshot.jpg");

var sampleMetadata = {
	collectionTime: new Date().getTime(),
	gatewayId: "000000000000",
	cameraId: "0",
	steps: [{
		name: "test",
		function: "motion",
		functionVersion: 1,
		configVersion:1,
		collectionInterval: 60000,
		inputParameters: {},
		startTime: new Date().getTime(),
		endTime: new Date().getTime(),
		output: {
			imageName: "snapshot.jpg"
		}
	}]
};

var sampleImages = [{
	name: "snapshot.jpg", 
	stepName: "test",
	data: testImageData
}];

function getTestFileName(){
	var name = "test-" + fileId + ".tar";
	fileId += 1;
	return name;
}

test.before(()=>{
	fse.ensureDirSync(testFileLocation);
})

test.after.always(()=>{
	fse.removeSync(testFileLocation);
});

test('stores to a tar archive', async t => {
	var path = testFileLocation + "/" + getTestFileName();

	await archiver.archive({metadata: sampleMetadata, images:sampleImages}, path);

	t.true(fse.pathExistsSync(path));

	//check that the archive is in the correct format
     await new Promise((resolve, reject) => {
     	var fileStream = fse.createReadStream(path);
     	var extract = tar.extract();

     	var count = 0;
     	extract.on("entry", (header, stream, next) => {

     		var chunks = [];
            stream.on('data', d => chunks.push(d));

            stream.on('end', () => {
            	var data = Buffer.concat(chunks);

            	if(count == 0){
	     			t.is(header.name, "data.json");
	     			t.deepEqual(JSON.parse(data.toString()), sampleMetadata);
	     		} else {
	     			//should check that these have the same content, but having the same length is probably good enough
	     			t.is(data.length, testImageData.length);
	     		}

            	count += 1;
     			next();
            });
     	});

     	extract.on("finish", () => {
     		t.is(count, 2);
     		resolve();
     	});

     	fileStream.pipe(extract);
     });

	t.pass();
});

test('rejects bad archives', async t =>{
	await t.throws(archiver.restore('./testData/badArchive.tar'));
});

test('stores to a tar archive from streamed images', async t => {
	var path = testFileLocation + "/" + getTestFileName();

	var imageStream = fse.createReadStream("./testData/20170814112430-snapshot.jpg");

	var sampleStreamedImage = {
		name: "snapshot.jpg", 
		stepName: "test",
		data: imageStream
	}

	await archiver.archive({metadata: sampleMetadata, images:[sampleStreamedImage]}, path);

	t.true(fse.pathExistsSync(path));

	//check that the archive is in the correct format
     await new Promise((resolve, reject) => {
     	var fileStream = fse.createReadStream(path);
     	var extract = tar.extract();

     	var count = 0;
     	extract.on("entry", (header, stream, next) => {

     		var chunks = [];
            stream.on('data', d => chunks.push(d));

            stream.on('end', () => {
            	var data = Buffer.concat(chunks);

            	if(count == 0){
	     			t.is(header.name, "data.json");
	     			t.deepEqual(JSON.parse(data.toString()), sampleMetadata);
	     		} else {
	     			//should check that these have the same content, but having the same length is probably good enough
	     			t.is(data.length, testImageData.length);
	     		}

            	count += 1;
     			next();
            });
     	});

     	extract.on("finish", () => {
     		t.is(count, 2);
     		resolve();
     	});

     	fileStream.pipe(extract);
     });

	t.pass();
});

test('stores to a tar archive from images from a file stream even if the stream was already consumed', async t => {
	var path = testFileLocation + "/" + getTestFileName();

	var imageStream = fse.createReadStream("./testData/20170814112430-snapshot.jpg");

	var sampleStreamedImage = {
		name: "snapshot.jpg", 
		stepName: "test",
		data: imageStream
	}

	//read the stream
	await new Promise((resolve, reject) => {
		sampleStreamedImage.data.on("data", ()=>{}).on("end", () => resolve());
	});

	await archiver.archive({metadata: sampleMetadata, images:[sampleStreamedImage]}, path);

	t.true(fse.pathExistsSync(path));

	//check that the archive is in the correct format
     await new Promise((resolve, reject) => {
     	var fileStream = fse.createReadStream(path);
     	var extract = tar.extract();

     	var count = 0;
     	extract.on("entry", (header, stream, next) => {

     		var chunks = [];
            stream.on('data', d => chunks.push(d));

            stream.on('end', () => {
            	var data = Buffer.concat(chunks);

            	if(count == 0){
	     			t.is(header.name, "data.json");
	     			t.deepEqual(JSON.parse(data.toString()), sampleMetadata);
	     		} else {
	     			//should check that these have the same content, but having the same length is probably good enough
	     			t.is(data.length, testImageData.length);
	     		}

            	count += 1;
     			next();
            });
     	});

     	extract.on("finish", () => {
     		t.is(count, 2);
     		resolve();
     	});

     	fileStream.pipe(extract);
     });

	t.pass();
});

test('other consumed streams cause an error', async t => {
	var path = testFileLocation + "/" + getTestFileName();

	var imageBuffer = fse.readFileSync("./testData/20170814112430-snapshot.jpg");
	var imageStream = new Readable();
	imageStream._read = function(){};
	imageStream.push(imageBuffer);
	imageStream.push(null);

	var sampleStreamedImage = {
		name: "snapshot.jpg", 
		stepName: "test",
		data: imageStream
	}

	//read the stream to consume it
	await new Promise((resolve, reject) => {
		sampleStreamedImage.data.on("data", (chunk)=>{}).on("end", () => resolve());
	});

	var msg = await t.throws( archiver.archive({metadata: sampleMetadata, images:[sampleStreamedImage]}, path));
	t.is(msg, "The content has already been consumed and cannot be reset");
});

test('restores from a tar archive', async t => {
	var path = testFileLocation + "/" + getTestFileName();

	//first create an archive
	await archiver.archive({metadata: sampleMetadata, images:sampleImages}, path);

	//then restore from it
	var restoredArchive = await archiver.restore(path);

	//check that everything in the result is the same as in the original
	t.deepEqual(restoredArchive.metadata, sampleMetadata);
	t.is(restoredArchive.images.length, 1);
	t.is(restoredArchive.images[0].name, "snapshot.jpg");
	t.is(restoredArchive.images[0].stepName, "test");
	t.is(restoredArchive.images[0].data.length, testImageData.length);
});