import test from 'ava';
import sinon from 'sinon';

import fs from "fs";
import moment from 'moment';
import request from 'request';

import API from '../api.js';

test("imageReading sends a multipart request", async t => {
	var src_img_file_path = "./testData/20170814112430-snapshot.jpg";

	var request_stub = sinon.stub(request,'post').callsFake( (parameters, callback) => {
		//check that the parameters are correct

		t.truthy(parameters.multipart);
		t.is(parameters.multipart.length, 2);

		t.is(parameters.multipart[0]["Content-Type"], "application/json");
		t.is(parameters.multipart[0]["Content-ID"], "<000000000000/0/1502735070/index@at0l.io>");

		var json = JSON.parse(parameters.multipart[0].body);
		t.is(json.collectionTime, 1502735070);
		t.is(json.gatewayId, "000000000000");
		t.is(json.cameraId, "0");
		t.is(json.steps.length, 1);
		t.is(json.steps[0].name, "test");

		t.is(parameters.multipart[1]["Content-Type"], "image/jpeg");
		t.is(parameters.multipart[1]["Content-ID"], "<000000000000/0/1502735070/test/snapshot.jpg@at0l.io>");
		t.is(parameters.multipart[1].body.length, 52828);

		callback();
	});

	await API.imageReading({
        metadata: {
            collectionTime: moment(1502735070000).unix(),
            gatewayId: "000000000000",
            cameraId: "0",
            steps: [{
                name: "test",
                function: "motion",
                version: 1,
                inputParameters: {},
                startTime: new Date().getTime(),
                endTime: new Date().getTime(),
                output: {
                    imageName: "snapshot.jpg"
                }
            }]
        }, 
        images:[{
            name: "snapshot.jpg", 
            stepName: "test",
            data: fs.readFileSync(src_img_file_path)
        }]
    });

	request_stub.restore();
});

test("imageReadings sends in batches", async t => {
	var src_img_file_path = "./testData/20170814112430-snapshot.jpg";

	var first = true;
	var request_stub = sinon.stub(request,'post').callsFake( (parameters, callback) => {
		//check that the parameters are correct

		if(first){
			t.truthy(parameters.multipart);
			t.is(parameters.multipart.length, 20);
			first = false
		} else {
			t.truthy(parameters.multipart);
			t.is(parameters.multipart.length, 4);
		}

		callback();
	});

	var data = fs.readFileSync(src_img_file_path);
	var testData = [0,1,2,3,4,5,6,7,8,9,10,11].map(idx => {
		return {
	        metadata: {
	            collectionTime: moment(1502735070000).unix(),
	            gatewayId: "000000000000",
	            cameraId: "0",
	            steps: [{
	                name: "test",
	                function: "motion",
	                version: 1,
	                inputParameters: {},
	                startTime: new Date().getTime(),
	                endTime: new Date().getTime(),
	                output: {
	                    imageName: "snapshot.jpg"
	                }
	            }]
	        }, 
	        images:[{
	            name: "snapshot.jpg", 
	            stepName: "test",
	            data: data
	        }]
	    }
	});

	await API.imageReadings(testData);

	sinon.assert.calledTwice(request_stub); //two batches sent
	t.is(testData.length, 12); //bulk upload should not alter the original array

	request_stub.restore();
});