import test from 'ava';
import sinon from 'sinon';
import fse from 'fs-extra';
import moment from 'moment';

import config from "../configuration.js";
import API from "../api.js";
import imageHandler from '../imageHandler.js';
import imageEventArchiver from '../imageEventArchiver.js';

test.after.always(()=>{
    fse.removeSync("./tmp");
});

test("generate a bundle from a legacy snapshot message", async t =>{
    var localTime = moment("20170814112430", "YYYYMMDDHHmmss");
    var basePath = "./testData/"
    var mqtt_message = "1:./testData/20170814112430-snapshot.jpg";

    var result = await imageHandler._parseLegacyMqttMessage(mqtt_message);

    t.is(result.metadata.cameraId, "1");
    t.is(result.metadata.collectionTime, localTime.unix());
    t.is(result.metadata.gatewayId, '000000000000');

    t.is(result.metadata.steps.length, 1);
    t.is(result.metadata.steps[0].name, "motion");
    t.is(result.metadata.steps[0]["function"], "snapshot");
    t.is(result.metadata.steps[0].output.cause, "snapshot");
    t.is(result.metadata.steps[0].startTime, localTime.toISOString());
    t.is(result.metadata.steps[0].configVersion, "xxxxxx");
    t.is(result.metadata.steps[0].collectionInterval, 60000);

    t.is(result.images.length, 1);
    t.is(result.images[0].name, "image.jpg");
    t.is(result.images[0].stepName, "motion");

    //simple test for whether the data is a stream
    //t.true(typeof result.images[0].data.pipe == "function");
    t.true(Buffer.isBuffer(result.images[0].data));
});

test("generate a bundle from a legacy message", async t =>{
    var localTime = moment("20170814112430", "YYYYMMDDHHmmss");
    var basePath = "./testData/"
    var mqtt_message = "1:./testData/20170814112430-00.jpg";

    var result = await imageHandler._parseLegacyMqttMessage(mqtt_message);

    t.is(result.metadata.cameraId, "1");
    t.is(result.metadata.collectionTime, localTime.unix());
    t.is(result.metadata.gatewayId, '000000000000');

    t.is(result.metadata.steps.length, 1);
    t.is(result.metadata.steps[0].name, "motion");
    t.is(result.metadata.steps[0]["function"], "motion");
    t.is(result.metadata.steps[0].output.cause, "00");
    t.is(result.metadata.steps[0].startTime, localTime.toISOString());
    t.is(result.metadata.steps[0].configVersion, "xxxxxx");
    t.is(result.metadata.steps[0].collectionInterval, undefined);

    t.is(result.images.length, 1);
    t.is(result.images[0].name, "image.jpg");
    t.is(result.images[0].stepName, "motion");

    //simple test for whether the data is a stream
    //t.true(typeof result.images[0].data.pipe == "function");
    t.true(Buffer.isBuffer(result.images[0].data));
});

test("generate a bundle from an mqtt message", async t =>{
    var mqtt_message = fse.readFileSync("./testData/testMqttMessage.txt");

    var result = await imageHandler._parseMqttMessage(mqtt_message);

    t.is(result.metadata.cameraId, 0);
    t.is(result.metadata.collectionTime, 1514918050);
    t.is(result.metadata.gatewayId, '0000aab65357');

    t.is(result.metadata.steps.length, 1);
    t.is(result.metadata.steps[0].name, "default");
    t.is(result.metadata.steps[0]["function"], "baseMetadata");
    t.is(result.metadata.steps[0].startTime, "2018-01-02T18:34:10.191Z");
    t.is(result.metadata.steps[0].collectionInterval, 60000);

    t.is(result.images.length, 1);
    t.is(result.images[0].name, "image.jpg");
    t.is(result.images[0].stepName, "default");

    //simple test for whether the data is a stream
    //t.true(typeof result.images[0].data.pipe == "function");
    t.true(Buffer.isBuffer(result.images[0].data));
});

test.serial("processImageBundle should write to backlog when API call fails", async t => {
    var api_stub = sinon.stub(API, "imageReading").callsFake(function () {
        return Promise.reject("simulated failure");
    });

    var src_img_file_path = "./testData/20170814112430-snapshot.jpg";

    var oldBacklogConfig = config.image_bundle_backlog_dir;
    config.image_bundle_backlog_dir = "./tmp/backlog_bundles";

    await imageHandler._processImageBundle({
        metadata: {
            collectionTime: moment(1502735070000).unix(),
            gatewayId: "000000000000",
            cameraId: "0",
            steps: [{
                name: "test",
                function: "motion",
                version: 1,
                inputParameters: {},
                startTime: new Date().getTime(),
                endTime: new Date().getTime(),
                output: {
                    imageName: "snapshot.jpg"
                }
            }]
        }, 
        images:[{
            name: "snapshot.jpg", 
            stepName: "test",
            data: fse.readFileSync(src_img_file_path)
        }]
    });

    t.true(fse.pathExistsSync('./tmp/backlog_bundles/CAM0-1502735070.tar'));
    imageHandler.clearBacklog();

    sinon.assert.calledOnce(api_stub);

    api_stub.restore();
    config.image_bundle_backlog_dir = oldBacklogConfig;

    t.pass()
});

test.serial("Legacy message handler should delete source image when API call succeeds", async t => {
    var api_stub = sinon.stub(API, "imageReading").callsFake(function () {
        return Promise.resolve({ message: "simulate success" });
    });

    var img_file_path = "./tmp/20170814112430-snapshot.jpg";
    var src_img_file_path = "./testData/20170814112430-snapshot.jpg";

    var localTime = moment("20170814112430",  "YYYYMMDDHHmmss");

    var oldBacklogConfig = config.image_bundle_backlog_dir;
    config.image_bundle_backlog_dir = "./tmp/backlog";

    //copy the test data to the working directory
    fse.copySync(src_img_file_path, img_file_path);
    t.true(fse.pathExistsSync(img_file_path));

    await imageHandler.processLegacyImageMessage("1:./tmp/20170814112430-snapshot.jpg");

    t.false(fse.pathExistsSync(img_file_path));
    sinon.assert.calledOnce(api_stub);

    //and it should not be in the backlog either
    t.false(fse.pathExistsSync('./tmp/backlog/CAM1-' + localTime.utc().unix() +'.tar'));

    api_stub.restore();
    config.image_bundle_backlog_dir = oldBacklogConfig;

    t.pass();
});

test.serial("Legacy message handler should delete source image when API call fails", async t => {
    var api_stub = sinon.stub(API, "imageReading").callsFake(function () {
        return Promise.reject("simulated failure");
    });

    var img_file_path = "./tmp/20170814112430-snapshot.jpg";
    var src_img_file_path = "./testData/20170814112430-snapshot.jpg";

    var localTime = moment("20170814112430",  "YYYYMMDDHHmmss");

    var oldBacklogConfig = config.image_bundle_backlog_dir;
    config.image_bundle_backlog_dir = "./tmp/backlog";

    //copy the test data to the working directory
    fse.copySync(src_img_file_path, img_file_path);
    t.true(fse.pathExistsSync(img_file_path));

    await imageHandler.processLegacyImageMessage("1:./tmp/20170814112430-snapshot.jpg");

    //source file was deleted
    t.false(fse.pathExistsSync(img_file_path));
    sinon.assert.calledOnce(api_stub);

    //it should be in the backlog though
    t.true(fse.pathExistsSync('./tmp/backlog/CAM1-' + localTime.utc().unix() +'.tar'));
    await imageHandler.clearBacklog();

    api_stub.restore();
    config.image_bundle_backlog_dir = oldBacklogConfig;

    t.pass();
});

test.serial("Legacy message handler should backlog content when the API call fails", async t => {
    var api_stub = sinon.stub(API, "imageReading").callsFake(function (bundle) {
        //simulate draining the streams
        var promises = bundle.images.map( imageEntry => {
            return new Promise((resolve, reject) => {
                if (imageEntry.data && typeof imageEntry.data === "object" && typeof imageEntry.data.pipe === "function" && imageEntry.data.readable !== false){
                    //drain the a stream
                    imageEntry.data.on("data", ()=>{
                    }).on("end", () => resolve(null) );
                } else {
                    resolve(null);
                }
            });
            
        });

        return Promise.all(promises).then(() =>{
            return Promise.reject("simulated failure");
        });
    });

    var img_file_path = "./tmp/20170814112430-snapshot.jpg";
    var src_img_file_path = "./testData/20170814112430-snapshot.jpg";

    var localTime = moment("20170814112430",  "YYYYMMDDHHmmss");

    var oldBacklogConfig = config.image_bundle_backlog_dir;
    config.image_bundle_backlog_dir = "./tmp/backlog";

    //copy the test data to the working directory
    fse.copySync(src_img_file_path, img_file_path);
    t.true(fse.pathExistsSync(img_file_path));

    await imageHandler.processLegacyImageMessage("1:./tmp/20170814112430-snapshot.jpg");

    //source file was deleted
    t.false(fse.pathExistsSync(img_file_path));
    sinon.assert.calledOnce(api_stub);

    //it should be in the backlog though
    var fileName = './tmp/backlog/CAM1-' + localTime.utc().unix() +'.tar';
    t.true(fse.pathExistsSync(fileName));
    
    //and that backlog should have content in it
    var restoredArchive = await imageEventArchiver.restore(fileName);
    t.is(restoredArchive.images.length, 1);
    t.is(restoredArchive.images[0].data.length, 52828);

    await imageHandler.clearBacklog();
    api_stub.restore();
    config.image_bundle_backlog_dir = oldBacklogConfig;

    t.pass();
});

test("toBatches creates batches", t => {
    var x = imageHandler._toBatches([1,2,3,4,5,6,7,8,9,10,11], 10);
    t.deepEqual(x, [[1,2,3,4,5,6,7,8,9,10], [11]]);
});

test("toBatches does not alter original array", t => {
    var originalArray = [1,2,3,4,5,6,7,8,9,10,11]
    var x = imageHandler._toBatches(originalArray, 10);
    t.deepEqual(x, [[1,2,3,4,5,6,7,8,9,10], [11]]);
    t.is(originalArray.length, 11);
});

test.serial("processBacklog should process the bundles in backlog dir", async t => {
    var original_image_backlog_dir = config.image_bundle_backlog_dir;
    var bundle_backlog_dir = "./tmp/backlog_bundles";
    config.image_bundle_backlog_dir = bundle_backlog_dir;

    //copy test data to backlog bundles
    fse.ensureDir(bundle_backlog_dir);
    [0,1,2].forEach( idx => {
        fse.copySync("./testData/backlog_bundles/test-"+idx+".tar", bundle_backlog_dir + "/test-" + idx +".tar");
    });

    var api_stub = sinon.stub(API, "imageReadings").callsFake(function () {
        return Promise.resolve({ message: "simulate success" });
    });
    
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-1.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-2.tar"));

    await imageHandler.processBacklog();

    t.is(api_stub.callCount, 1); //all three files are uploaded in one request
    
    //none of the files should still exist
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-1.tar"));
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-2.tar"));

    api_stub.restore();

    t.pass();

    config.image_bundle_backlog_dir = original_image_backlog_dir;
});

test.serial("processBacklog should not delete bundles if the upload fails", async t => {
    var original_image_backlog_dir = config.image_bundle_backlog_dir;
    var bundle_backlog_dir = "./tmp/backlog_bundles";
    config.image_bundle_backlog_dir = bundle_backlog_dir;

    //copy test data to backlog bundles
    fse.ensureDir(bundle_backlog_dir);
    [0,1,2].forEach( idx => {
        fse.copySync("./testData/backlog_bundles/test-"+idx+".tar", bundle_backlog_dir + "/test-" + idx +".tar");
    });

    var api_stub = sinon.stub(API, "imageReadings").callsFake(function () {
        return Promise.reject("simulated failure");
    });
    
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-1.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-2.tar"));

    await imageHandler.processBacklog();

    t.is(api_stub.callCount, 1); //all three files are uploaded in one request
    
    //all of the files should still exist
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-1.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-2.tar"));

    await imageHandler.clearBacklog();

    api_stub.restore();

    t.pass();

    config.image_bundle_backlog_dir = original_image_backlog_dir;
});

test.serial("processBacklog should only delete bundles for successful uploads", async t => {
    var original_image_backlog_dir = config.image_bundle_backlog_dir;
    var bundle_backlog_dir = "./tmp/backlog_bundles";
    config.image_bundle_backlog_dir = bundle_backlog_dir;

    //copy test data to backlog bundles
    fse.ensureDir(bundle_backlog_dir);
    [0,1,2,3].forEach(group => {
        [0,1,2].forEach( idx => {
            fse.copySync("./testData/backlog_bundles/test-"+idx+".tar", bundle_backlog_dir + "/test-" + ((group*3) + idx) +".tar");
        });
    });

    var callCount = 0;
    var api_stub = sinon.stub(API, "imageReadings").callsFake(function () {
        //succeed the first time
        if (callCount == 0){
            callCount += 1;
            return Promise.resolve({ message: "simulate success" });
        }
        //fail the second
        callCount += 1;
        return Promise.reject("simulated failure");
    });
    
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-11.tar"));

    await imageHandler.processBacklog();

    t.is(api_stub.callCount, 2); //two batches
    
    //all of the files should still exist
    var existCount = 0;
    [0,1,2,3].forEach(group => {
        [0,1,2].forEach( idx => {
            existCount += (fse.pathExistsSync( bundle_backlog_dir + "/test-" + ((group*3) + idx) +".tar")) ? 1 : 0;
        });
    });

    t.is(existCount, 2); //10 succeeded and 2 failed. Which exact ones are undefined.

    await imageHandler.clearBacklog();

    api_stub.restore();

    t.pass();

    config.image_bundle_backlog_dir = original_image_backlog_dir;
});

test.serial("processBacklog should skip and delete invalid archives", async t => {
    var original_image_backlog_dir = config.image_bundle_backlog_dir;
    var bundle_backlog_dir = "./tmp/backlog_bundles";
    config.image_bundle_backlog_dir = bundle_backlog_dir;

    //copy test data to backlog bundles
    fse.ensureDir(bundle_backlog_dir);
    [0,1,2].forEach( idx => {
        fse.copySync("./testData/backlog_bundles/test-"+idx+".tar", bundle_backlog_dir + "/test-" + idx +".tar");
    });

    fse.copySync("./testData/badArchive.tar", bundle_backlog_dir + "/test-3.tar");

    var api_stub = sinon.stub(API, "imageReadings").callsFake(function (parameters) {
        t.is(parameters.length, 3);
        return Promise.resolve({ message: "simulate success" });
    });
    
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-1.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-2.tar"));
    t.true(fse.pathExistsSync(bundle_backlog_dir +"/test-3.tar"));

    await imageHandler.processBacklog();

    t.is(api_stub.callCount, 1); //all three files are uploaded in one request
    
    //none of the files should still exist
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-0.tar"));
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-1.tar"));
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-2.tar"));
    t.false(fse.pathExistsSync(bundle_backlog_dir +"/test-3.tar"));

    api_stub.restore();

    t.pass();

    config.image_bundle_backlog_dir = original_image_backlog_dir;
});

test("clearBacklog removes all backlog files", async t=>{
    var srcBundleFilePath = "./testData/backlog_bundles/test-0.tar";

    var original_image_backlog_dir = config.image_bundle_backlog_dir;
    var bundle_backlog_dir = "./tmp/backlog_bundles";
    config.image_bundle_backlog_dir = bundle_backlog_dir;

    fse.copySync(srcBundleFilePath, bundle_backlog_dir + "/test-0.tar");

    t.true(fse.pathExistsSync(bundle_backlog_dir + "/test-0.tar"));

    await imageHandler.clearBacklog();

    t.false(fse.pathExistsSync(bundle_backlog_dir + "/test-0.tar"));

    config.image_bundle_backlog_dir = original_image_backlog_dir;
});

test("clearBacklog  with a date removes all backlog files before that date", async t=>{
    var srcBundleFilePath = "./testData/backlog_bundles/test-0.tar";

    var original_image_backlog_dir = config.image_bundle_backlog_dir;
    var bundle_backlog_dir = "./tmp/backlog_bundles";
    config.image_bundle_backlog_dir = bundle_backlog_dir;

    var filePathToRemove = bundle_backlog_dir + "/test-1502735070.tar";
    var filePathToKeep = bundle_backlog_dir + "/test-1502750000.tar";

    fse.copySync(srcBundleFilePath, filePathToRemove);
    fse.copySync(srcBundleFilePath, filePathToKeep);

    t.true(fse.pathExistsSync(filePathToRemove));
    t.true(fse.pathExistsSync(filePathToKeep));

    await imageHandler.clearBacklog(new Date(1502740000000));

    t.false(fse.pathExistsSync(filePathToRemove));
    t.true(fse.pathExistsSync(filePathToKeep));

    await imageHandler.clearBacklog();

    config.image_bundle_backlog_dir = original_image_backlog_dir;
});
