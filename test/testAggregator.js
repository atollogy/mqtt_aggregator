import test from 'ava'
import sinon from 'sinon'
import fse from 'fs-extra'
import zlib from 'zlib'

import aggregator from '../aggregator.js'
import API from '../api.js'

test("makeSlot converts a unix timestamp to the start of the minute", t => {
    t.is(aggregator._makeSlot(1499900998), 1499900940);
    t.is(aggregator._makeSlot(1499900997), 1499900940);
});

test("aggregateGWData", t =>{
	var data = [{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      },{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900998,
        "rssi": -81,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

	var result = aggregator.aggregateGWData(data);

	t.is(result.avg_rssi, -80);
	t.is(result.mac, "e2020008b440");
    t.is(result.namespace, "f7826da64fa24e988024bc5b71e0893e");
    t.is(result.beacon_id, "e7723927");
	t.is(result.gateway_id, "b827eb15a6a6");
    t.is(result.slot, 1499900940);
    t.is(result.slot_raw_data.length, 2);
    t.is(result.tx_power_1m, -65);
    t.is(result.best_rssi, -79);
    t.is(result.reading_count, 2);
    t.is(result.sd_rssi, 1);
    t.is(Math.round(result.sd_distance *1000)/1000, .649);
    t.not(result.avg_distance, undefined);
    t.not(result.distance_proximity, undefined);
});

test("aggregateGWData handles zero rssi", t =>{
    var data = [{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900996,
        "rssi": 0,
        "tx_power_1m": 4,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      },{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": 0,
        "tx_power_1m": 4,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    var result = aggregator.aggregateGWData(data);

    t.is(result.avg_rssi, 0);
    t.is(result.mac, "e2020008b440");
    t.is(result.gateway_id, "b827eb15a6a6");
});


test("store() writes to cache", t => {
    var beaconData =  {
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      };

    aggregator.store(beaconData);

    t.truthy(aggregator._cache[1499900940]);
    aggregator._clear();
});

test("store() rejects values with invalid rssi or txpower", t => {
    var beaconData =  [{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": 62,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      },{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": 21,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      },{
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -150,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    beaconData.forEach(aggregator.store);

    t.falsy(aggregator._cache[1499900940]);
    aggregator._clear();
});

test("aggregateCompletedSlots waits to aggregate older data only", t => {
    var beaconData = [ {
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    beaconData.forEach(aggregator.store);
    var result = aggregator.aggregateCompletedSlots(new Date(1499900940000));

    t.is(result.length, 0);
    aggregator._clear();
});

test("aggregateCompletedSlots aggregates data", t => {
    var beaconData = [ {
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    beaconData.forEach(aggregator.store);
    var aggTime = new Date((new Date()).getTime() + 61*1000);
    var result = aggregator.aggregateCompletedSlots(aggTime);

    t.is(result.length, 1);
});


test("aggregateCompletedSlots aggregates several beacons", t => {
    var beaconData = [ 
        {"beacon_type":"eddystone","beacon_subtype":"uid","instance":"f2124ad2244d","namespace":"edd1ebeac04e5defa017","tx_power_1m":-72,"rssi":-83,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["f2124ad2244d","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"fe8ed1644012","namespace":"b9407f30f5f8466eaff925556b57fe6d","tx_power_1m":-77,"rssi":-94,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["fe8ed1644012","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"ccad8f75f9d3","namespace":"e9b277f55b87453d8873af2403784254","tx_power_1m":-73,"rssi":-75,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["ccad8f75f9d3","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"c90ec1c0015b","namespace":"f7826da64fa24e988024bc5b71e089ff","tx_power_1m":-59,"rssi":-68,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["c90ec1c0015b","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"e20200081c40","namespace":"f7826da64fa24e988024bc5b71e0893e","tx_power_1m":-65,"rssi":-79,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["e20200081c40","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"e20200079e40","namespace":"f7826da64fa24e988024bc5b71e0893e","tx_power_1m":-65,"rssi":-62,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["e20200079e40","b827eb20ca00"],"lastSeen":1502410055}
    ];

    beaconData.forEach(aggregator.store);
    var aggTime = new Date((new Date()).getTime() + 61*1000);
    var result = aggregator.aggregateCompletedSlots(aggTime);

    t.is(result.length, 6);
});

test("aggregateCompletedSlots aggregates several beacons with multiple advertisements", t => {
    var beaconData = [ 
        {"beacon_type":"eddystone","beacon_subtype":"uid","instance":"f2124ad2244d","namespace":"edd1ebeac04e5defa017","tx_power_1m":-72,"rssi":-83,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["f2124ad2244d","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"fe8ed1644012","namespace":"b9407f30f5f8466eaff925556b57fe6d","tx_power_1m":-77,"rssi":-94,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["fe8ed1644012","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"ccad8f75f9d3","namespace":"e9b277f55b87453d8873af2403784254","tx_power_1m":-73,"rssi":-75,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["ccad8f75f9d3","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"c90ec1c0015b","namespace":"f7826da64fa24e988024bc5b71e089ff","tx_power_1m":-59,"rssi":-68,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["c90ec1c0015b","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"e20200081c40","namespace":"f7826da64fa24e988024bc5b71e0893e","tx_power_1m":-65,"rssi":-79,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["e20200081c40","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"e20200079e40","namespace":"f7826da64fa24e988024bc5b71e0893e","tx_power_1m":-65,"rssi":-62,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["e20200079e40","b827eb20ca00"],"lastSeen":1502410055},
        {"beacon_type":"eddystone","beacon_subtype":"uid","instance":"f2124ad2244d","namespace":"edd1ebeac04e5defa017","tx_power_1m":-72,"rssi":-83,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["f2124ad2244d","b827eb20ca00"],"lastSeen":1502410057},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"fe8ed1644012","namespace":"b9407f30f5f8466eaff925556b57fe6d","tx_power_1m":-77,"rssi":-94,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["fe8ed1644012","b827eb20ca00"],"lastSeen":1502410057},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"ccad8f75f9d3","namespace":"e9b277f55b87453d8873af2403784254","tx_power_1m":-73,"rssi":-75,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["ccad8f75f9d3","b827eb20ca00"],"lastSeen":1502410057},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"c90ec1c0015b","namespace":"f7826da64fa24e988024bc5b71e089ff","tx_power_1m":-59,"rssi":-68,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["c90ec1c0015b","b827eb20ca00"],"lastSeen":1502410057},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"e20200081c40","namespace":"f7826da64fa24e988024bc5b71e0893e","tx_power_1m":-65,"rssi":-79,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["e20200081c40","b827eb20ca00"],"lastSeen":1502410057},
        {"beacon_type":"iBeacon","beacon_subtype":"","instance":"e20200079e40","namespace":"f7826da64fa24e988024bc5b71e0893e","tx_power_1m":-65,"rssi":-62,"gateway_id":"b827eb20ca00","nodename":"atlgw00","provenance":["e20200079e40","b827eb20ca00"],"lastSeen":1502410057}
    ];

    beaconData.forEach(aggregator.store);
    var aggTime = new Date((new Date()).getTime() + 61*1000);
    var result = aggregator.aggregateCompletedSlots(aggTime);

    t.is(result.length, 6);
    t.is(result[0].slot_raw_data.length, 2);
});


test.serial("upload beacon data", async t =>{

    var api_stub = sinon.stub(API, "beaconReadings").callsFake(function(aggData){
        t.is(aggData.length, 1);
        t.is(aggData[0].slot, 1499900940);
        t.is(aggData[0].slot_raw_data.length, 1);
        t.is(aggData[0].avg_rssi, -79);
        t.is(aggData[0].tx_power_1m, -65);
    	return Promise.resolve({message:"stub insert"});
    });

    var beacon_data = [ {
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    beacon_data.forEach(aggregator.store);

    var aggTime = new Date((new Date()).getTime() + 61*1000);
    aggregator.upload(aggTime);

    api_stub.restore();

    sinon.assert.calledOnce(api_stub);
    t.pass()
});

test.serial("adds to backlog on failure", async t =>{

    var api_stub = sinon.stub(API, "beaconReadings").callsFake(function(aggData){
        return Promise.reject({message:"stub insert"});
    });

    var beacon_data = [ {
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    beacon_data.forEach(aggregator.store);

    var aggTime = new Date((new Date()).getTime() + 61*1000);
    
    return aggregator.upload(aggTime).then(function(){
        sinon.assert.calledOnce(api_stub);
        t.is(aggregator._backlog.length, 1);

        api_stub.restore();

        //clear out this backlog entry
        aggregator._clear();
        t.pass();
    });
});

test.serial("uploads from backlog", async t =>{

    var api_stub = sinon.stub(API, "beaconReadings").callsFake(function(aggData){
        t.is(aggData.length, 1);
        return Promise.resolve({message:"stub insert"});
    });

    var beacon_data = [ {
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      }];

    var aggregatedData = aggregator.aggregateGWData(beacon_data);
    aggregator._backlog.push(aggregatedData);
    
    return aggregator.upload().then(function(){
        sinon.assert.calledOnce(api_stub);
        t.is(aggregator._backlog.length, 0);

        api_stub.restore();
        t.pass()
    });
});

test.serial("archive and restore", t =>{
    var location = "./work";
    t.is(Object.keys(aggregator._cache).length, 0);
    t.is(aggregator._backlog.length, 0);

    //restore stuff
    fse.outputFileSync(location+ "/archive_1.json", JSON.stringify({version: aggregator._ARCHIVE_VERSION, cache: {"slotKey":{gateways:{"gatewayId":{"beaconId":[]}}}}, backlog:[{x:"x"}]}));
    aggregator.restore(location);

    t.is(Object.keys(aggregator._cache).length, 1);
    t.true(aggregator._cache["slotKey"].updatedTime instanceof Date);
    t.is(aggregator._backlog.length, 1);
    t.true(aggregator._backlog[0].backlog);
    t.falsy(fse.existsSync(location + "/archive_1.json"));

    //archive the current state
    aggregator.archive(location);

    t.truthy(fse.existsSync(location+ "/archive_1.json.gz"));
    var writtenArchive = JSON.parse(zlib.unzipSync(fse.readFileSync(location+ "/archive_1.json.gz")));
    t.truthy(writtenArchive.cache.slotKey);
    t.is(writtenArchive.backlog.length, 1);
    t.truthy(writtenArchive.backlog[0].x);

    //clean up
    aggregator._clear();
    fse.removeSync(location);
});

test.serial("restore should not recover from a different archive version", t =>{
    var location = "./work";
    t.is(Object.keys(aggregator._cache).length, 0);
    t.is(aggregator._backlog.length, 0);

    //restore stuff
    fse.outputFileSync(location+ "/archive_1.json", JSON.stringify({version: 0, cache: {"slotKey":{gateways:{"gatewayId":{"beaconId":[]}}}}, backlog:[{x:"x"}]}));
    aggregator.restore(location);

    t.is(Object.keys(aggregator._cache).length, 0);
    t.is(aggregator._backlog.length, 0);
    t.falsy(fse.existsSync(location + "/archive_1.json"));

    //clean up
    aggregator._clear();
});

test.serial("restore should read zip files", t =>{
    var location = "./work";
    t.is(Object.keys(aggregator._cache).length, 0);
    t.is(aggregator._backlog.length, 0);

    //restore stuff
    fse.outputFileSync(location+ "/archive_1.json.gz", zlib.gzipSync(JSON.stringify({version: aggregator._ARCHIVE_VERSION, cache: {"slotKey":{gateways:{"gatewayId":{"beaconId":[]}}}}, backlog:[{x:"x"}]})));
    aggregator.restore(location);

    t.is(Object.keys(aggregator._cache).length, 1);
    t.true(aggregator._cache["slotKey"].updatedTime instanceof Date);
    t.is(aggregator._backlog.length, 1);
    t.true(aggregator._backlog[0].backlog);
    t.falsy(fse.existsSync(location + "/archive_1.json"));

    //clean up
    aggregator._clear();
    fse.removeSync(location);
});

test.serial("archive should include entries in progress", t => {
    var location = "./work";
    var api_stub = sinon.stub(API, "beaconReadings").callsFake(function(aggData){
        return new Promise((resolve, reject) => {}); //this will never return, simulating a long request
    });

    aggregator.store({
        "mac":"e2020008b440",
        "gateway_id": "b827eb15a6a6",
        "nodename": "atlgw00-b827eb15a6a6_atl_dev",
        "provenance": [
          "e2020008b440",
          "b827eb15a6a6",
          "atlgw00-b827eb15a6a6_atl_dev"
        ],
        "lastSeen": 1499900997,
        "rssi": -79,
        "tx_power_1m": -65,
        "beacon_type": "iBeacon",
        "beacon_subtype": "",
        "tlm_temp": 0,
        "tlm_vbatt": 0,
        "url": "",
        "namespace": "f7826da64fa24e988024bc5b71e0893e",
        "instance": "e7723927"
      });
  
    var aggTime = new Date((new Date()).getTime() + 61*1000);
    aggregator.upload(aggTime);
    aggregator.archive(location);

    t.truthy(fse.existsSync(location+ "/archive_1.json.gz"));
    var writtenArchive = JSON.parse(zlib.unzipSync(fse.readFileSync(location+ "/archive_1.json.gz")));
    t.is(writtenArchive.backlog.length, 1);

    aggregator._clear();
    fse.removeSync(location);
});
