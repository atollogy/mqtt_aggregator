#!/usr/bin/env node

//******************************************************************************
// Publish the MAC addresses of all sniffed BLE packets.
//******************************************************************************

var debug = require('debug')('mqtt_aggregator');
var mqtt = require('mqtt');
var schedule = require('node-schedule');
var moment = require("moment");

var configuration = require("./configuration");
var aggregator = require("./aggregator.js");
var imageHandler = require("./imageHandler.js");

//******************************************************************************
// CONFIGURATION OPTIONS
//******************************************************************************;
var _mqtt_client = mqtt.connect(configuration.mqttServer);

// MQTT Topics
const TOPIC_PREFIX = `atollogy/ble/#`;
const IMG_TOPIC_PREFIX = `atollogy/img`;
const IMG_BUNDLE_TOPIC_PREFIX = `atollogy/imageBundle`;

//******************************************************************************
// MAIN CODE
//******************************************************************************

// MQTT connect callback to subscribe to beacon feeds
_mqtt_client.once('connect', function () {
    console.log("Aggregator MQTT client connected, subscribing to topics.");
    _mqtt_client.subscribe(TOPIC_PREFIX);
    _mqtt_client.subscribe(IMG_TOPIC_PREFIX);
    _mqtt_client.subscribe(IMG_BUNDLE_TOPIC_PREFIX);
});

// MQTT message handler
_mqtt_client.on('message', function (topic, message) {
    if(debug.enabled){
        debug("Message Received on: " + topic + " | " + message.toString());
    }

    if(topic.startsWith(TOPIC_PREFIX.substring(0,TOPIC_PREFIX.length - 1))){
        handleMqttBeaconData(topic, message);
    }else if(topic.startsWith(IMG_TOPIC_PREFIX)){
        handleMqttImageData(message);
    } else if (topic.startsWith(IMG_BUNDLE_TOPIC_PREFIX)){
        handleMqttImageBundleData(message);
    }
});

function handleMqttBeaconData(topic, message){
    var topic_parts = topic.split('/');
    var adv = JSON.parse(message.toString());
    if (typeof adv === 'undefined' || !adv) {
        return;
    }

    adv.provenance.push(configuration.nodename);

    aggregator.store(adv);
}

var receivingImages = false;
function handleMqttImageData(message){
    receivingImages = true;
    imageHandler.processLegacyImageMessage(message);
}
function handleMqttImageBundleData(message){
    receivingImages = true;
    imageHandler.processImageMessage(message);
}

function processSlot() {
    debug("scheduled slot processing is starting.");
    aggregator.upload().then(function(){
        if(receivingImages){
            return imageHandler.processBacklog();
        }  
    });
}

function getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var slotTime = new schedule.RecurrenceRule();
slotTime.second = getRandomInt(5,55);
var slotJob = schedule.scheduleJob(slotTime, processSlot);

//archive on exit and restore on launch
aggregator.restore(configuration.archiveLocation);

process.on("exit", function(){
    aggregator.archive(configuration.archiveLocation);
});

process.on("SIGTERM", function(){
    console.log("aggregator received sigterm");
    process.exit(0);
});

process.on("SIGINT", function(){
    console.log("aggregator received sigint");
    process.exit(0);
});

process.on("SIGHUP", function(){
    console.log("aggregator received sighup");
});

// clear very old backlog files
imageHandler.clearBacklog(moment().subtract(6,"day").startOf("day"));

console.log("MQTT Aggregator Started");
