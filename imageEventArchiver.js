var fs = require('fs-extra');
var tar = require('tar-stream');

async function saveEntryToTarFile(entry, pathToFile){
    var pack = tar.pack();
    var outputStream = fs.createWriteStream(pathToFile);

    var result = new Promise((resolve, reject) => {
        outputStream.on('error', (e) =>{
            console.log(e);
            reject(e);
        });
        outputStream.on('close', () => resolve() );
    });

    async function addEntry(name, content){
        return new Promise( (resolve, reject) => {
            if (content && typeof content === "object" && typeof content.pipe === "function"){
                //handle as a stream
                if (content.readable === false){
                    //stream may already have been consumed, so create a new one if we can
                    if (content.path){
                        var newStream = fs.createReadStream(content.path);
                        content = newStream;
                    } else {
                        throw("The content has already been consumed and cannot be reset");
                    }
                }

                var chunks = [];
                content.on('data', d => chunks.push(d));
                content.on('end', () => {
                    var contentBuffer = Buffer.concat(chunks);
                    pack.entry({name:name}, contentBuffer, (err)=>{
                        if(err){
                            reject(err);
                        } else {
                            resolve(null);
                        }
                    });
                });
            } else {
                //handle as a buffer
                pack.entry({name:name}, content, (err)=>{
                    if(err){
                        reject(err);
                    } else {
                        resolve(null);
                    }
                });
            }
        });
    }

    var promises = [];
    promises.push(addEntry("data.json", JSON.stringify(entry.metadata)));

    entry.images.forEach(image => {
        var name = (image.stepName) ? image.stepName + "/" + image.name : image.name;
        promises.push(addEntry(name, image.data));
    });

    await Promise.all(promises);

    pack.finalize();

    pack.pipe(outputStream);

    return result;
}

async function readEntryFromTarFile(pathToFile){
    return new Promise(function(resolve, reject){
        var fileStream = fs.createReadStream(pathToFile);
        var extract = tar.extract();

        var outputEntry = {images:[]};

        var first = true;
        extract.on('entry', (header, stream, next) => {
            var chunks = [];
            stream.on('data', d => chunks.push(d));

            stream.on('end', () => {
                var data = Buffer.concat(chunks);

                if (first && header.name == "data.json"){
                    outputEntry.metadata = JSON.parse(data);
                    first = false;
                } else {
                    var nameParts = header.name.split("/");
                    var name = nameParts.pop();
                    var stepName = nameParts.length ? nameParts.join("/") : undefined;

                    outputEntry.images.push({
                        name: name,
                        stepName: stepName,
                        data: data
                    });
                }

                next();
            });
        });

        extract.on('finish', ()=>{
            if(!outputEntry.metadata){
                reject("archive at " + pathToFile + " is invalid");
            } else{
                resolve(outputEntry);
            }
        });

        fileStream.pipe(extract);
    });
}

module.exports = {
    archive: saveEntryToTarFile,
    restore: readEntryFromTarFile
}