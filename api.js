var request = require("request");
var zlib = require("zlib");
var fs = require("fs");

const configuration = require("./configuration.js");

var REQUEST_TIMEOUT = 5*60*1000;

function beaconReadings(data){
    return (new Promise(function(resolve, reject){
        var uncompressed = JSON.stringify(data || {});
        zlib.gzip(uncompressed, function(error, result){
            if(error){
                reject(error);
            } else {
                resolve(result);
            }
        });
    })).then(function(compressedData){
        return new Promise(function(resolve, reject){
            request.post({
                url: `${configuration.apiProtocol}://${configuration.apiHost}/${configuration.apiVersion}/beacon_readings`,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Encoding' : 'gzip',
                    "customer_id": configuration.customerId,
                    "Gateway_Id": configuration.gateway_id,
                    "nodename": configuration.nodename
                },
                timeout: REQUEST_TIMEOUT,
                body: compressedData
            }, function(error, response, body){
                if(error){
                    reject(error);
                    return;
                }
                if(response && response.statusCode >= 300){
                    reject("Error: " + response.statusCode + ": " + response.statusMessage);
                    return;
                }
                resolve(body);
            });

        });
    });
}

async function imageReadings(bundleList){
    //validate
    if (bundleList.length == 0) {
        return Promise.reject("No readings bundles supplied");
    }

    for (var i in bundleList) {
        var entry = bundleList[i];
        if (!entry.metadata || !entry.metadata.gatewayId || entry.metadata.cameraId === undefined || !entry.metadata.collectionTime){
            return Promise.reject("Image reading bundle is missing identifying information");
        }
        for (var j in entry.images){
            var imageInfo = entry.images[j];
            if (!imageInfo.stepName || !imageInfo.name){
                return Promise.reject("An image in the bundle is missing identifying information");
            }
        }
    }

    //construct the multipart request
    var parts = [];

    bundleList.forEach( entry => {
        parts.push({
            "Content-Type": "application/json",
            "Content-ID": `<${entry.metadata.gatewayId}/${entry.metadata.cameraId}/${entry.metadata.collectionTime}/index@at0l.io>`,
            body: JSON.stringify(entry.metadata)
        });

        entry.images.forEach(image =>{
            parts.push({
                "Content-Type": "image/jpeg",
                "Content-ID": `<${entry.metadata.gatewayId}/${entry.metadata.cameraId}/${entry.metadata.collectionTime}/${image.stepName}/${image.name}@at0l.io>`,
                "Content-Disposition": `attachment; filename="${image.stepName}/${image.name}.jpg"`,
                body: image.data
            })
        });
    });

    //send the request
    return new Promise(function(resolve, reject){
        request.post({
            url: `${configuration.apiProtocol}://${configuration.apiHost}/${configuration.apiVersion}/image_reading`,
            timeout: REQUEST_TIMEOUT,
            headers: {
               "customer_id": configuration.customerId,
               "Gateway_Id": configuration.gateway_id,
               "nodename": configuration.nodename
            },
            multipart: parts
        },
        function(error, response, body){
            if(error){
                reject(error);
                return;
            }
            if(response && response.statusCode >= 300){
                reject("Error: " + response.statusCode + ": " + response.statusMessage);
                return;
            }
            resolve(body);
        });
    });
}

async function imageReading(bundle){
    return imageReadings([bundle]);
}

async function bulkImageUpload(bundleList){
    //if we're going to alter the incoming array, we need to make a copy
    var toSend = (bundleList.length > 10) ? Array.from(bundleList) : bundleList;

    while (toSend.length > 10){
        //batch in groups of 10 and send
        var batch = toSend.splice(0,10);
        await imageReadings(batch);
    }
    if (toSend.length){
        await imageReadings(toSend);
    }
}

module.exports = {
    beaconReadings: beaconReadings,
    imageReading: imageReading,
    imageReadings: bulkImageUpload
}
