const fse = require('fs-extra');
var moment = require('moment');
var recursive = require("recursive-readdir");
var API = require("./api.js");
var config = require("./configuration.js");
var imageEventArchiver = require("./imageEventArchiver.js");

async function parseLegacyMqttMessage(message) {
    var parts = message.toString().split(":");
    var cam = parts[0];
    var imgPath = parts[1];
    var imgPathParts = imgPath.split("/");
    var imageName = imgPathParts[imgPathParts.length - 1];
    var cause = imageName.split("-")[1].split(".")[0];

    var collectionMoment = moment(imageName.split("-")[0], "YYYYMMDDHHmmss");

    var imageData = await fse.readFile(imgPath);

    return {
        metadata: {
            collectionTime: collectionMoment.unix(),
            gatewayId: config.gateway_id,
            cameraId: cam,
            steps: [{
                name: "motion",
                "function": (cause == "snapshot") ? "snapshot" : "motion",
                functionVersion: 0,
                configVersion: config.motionConfigVersion,
                collectionInterval: (cause == 'snapshot')? config.motionSnapshotInterval : undefined,
                inputParameters: {},
                startTime: collectionMoment.toISOString(),
                endTime: moment().toISOString(),
                output: {
                    cause: cause
                }
            }]
        },
        images: [{
            name: "image.jpg",
            stepName: "motion",
            data: imageData
        }]
    };
}

async function parseMqttMessage(message){
    var incomingMessage = JSON.parse(message);

    return {
        metadata: {
            collectionTime: moment(incomingMessage.collectionTime).unix(),
            gatewayId: incomingMessage.gatewayId, 
            cameraId: incomingMessage.cameraId,
            steps: incomingMessage.steps
        },
        images: incomingMessage.images.map(imageInfo => {
            return {
                name: imageInfo.name,
                stepName: imageInfo.stepName,
                data: Buffer.from(imageInfo.data, "base64")
            }
        })
    };
}

async function processImageBundle(bundle) {
    try {
        await API.imageReading(bundle);
        console.log("Image bundle uploaded (images: " + ((bundle.images && bundle.images.length) || 0) + ")");
    } catch (imageReadingError) {
        var fileName = "CAM" + bundle.metadata.cameraId + "-" + bundle.metadata.collectionTime + ".tar";
        try {
            await fse.ensureDir(config.image_bundle_backlog_dir);
            //write image bundle to a file at `${image_bundle_backlog_dir}/${filename}`
            console.log("Image bundle submission failed, writing: " + fileName + ". Error was: " + imageReadingError);
            await imageEventArchiver.archive(bundle, config.image_bundle_backlog_dir + "/" + fileName);
        } catch (camFileCreationError) {
            console.error('There was an unexpected error while trying to write the CAM file.');
            console.error('The CAM file creation error is:', camFileCreationError);
            console.error('The Image reading error is:', imageReadingError);
        }
    }
}

async function processImageMessage(message) {
    var bundle = await parseMqttMessage(message);
    return processImageBundle(bundle);
}

async function processLegacyImageMessage(message){
    var bundle = await parseLegacyMqttMessage(message);
    await processImageBundle(bundle);

    //remove the original image
    fse.removeSync(message.toString().split(":")[1]);
}

// returns an array of arrays, each with up to batchSize elements
function toBatches(array, batchSize){
    var toBatch = Array.from(array);
    var output = [];

    while (toBatch.length > batchSize){
        //batch in groups of 10
        output.push(toBatch.splice(0,batchSize));
    }
    if (toBatch.length){
        output.push(toBatch);
    }

    return output
}

var backlogRunning = false;
async function processBacklog() {
    if(backlogRunning){
        console.log("Skipping backlog processing because an earlier run is still in progress");
        return null;
    }

    backlogRunning = true;
    console.log(`Handling backlog images in dir ${config.image_bundle_backlog_dir}`);

    var successCount = 0;
    var skipCount = 0;
    try {
        var files = await recursive(config.image_bundle_backlog_dir);

        if (files.length){
            //create batches
            var batches = toBatches(files, 10);

            for (var i in batches){
                var batch = batches[i];
                var entries = await Promise.all(batch.map( (filePath) => {
                    return imageEventArchiver.restore(filePath).catch((e) => {
                        console.log("skipping bad archive in backlog: " + filePath);
                    })
                }));

                entries = entries.filter(entry => entry);

                if(entries.length){
                    await API.imageReadings(entries);
                }
                
                successCount += entries.length;
                skipCount += (batch.length - entries.length);
                await Promise.all(batch.map( x => fse.remove(x) ));
            }
            console.log('successfully uploaded ' + successCount + ' backlog image bundles' + ((skipCount)? " (skipped "+skipCount+")" : ""));
        }
        
    } catch (err){
         console.error(`error submitting backlog images: ${err}`);
         console.log(`${successCount} items were uploaded successfully before the error` + ((skipCount)? " (skipped "+skipCount+")" : ""));
    }
    backlogRunning = false;
}

async function clearBacklog(beforeDate){
    if (!fse.existsSync(config.image_bundle_backlog_dir)){
        return;
    }

    var files = await recursive(config.image_bundle_backlog_dir);

    if (beforeDate) {
        files = files.filter( name => {
            var fileName = name.split(".")[0];
            if (!fileName) return true;

            var dateString = fileName.split("-")[1];
            if (!dateString) return true;

            var num = parseInt(dateString);
            var date = moment.unix(num);

            return !date.isValid() || date.isBefore(beforeDate);
        });
    }

    if(files.length == 0){
        return;
    }

    console.log("clearing backlog files: " + files.length);
    return Promise.all(files.map(function(file){
        console.log("removing " + file);
        return fse.remove(file);
    }));
}

module.exports = {
    _toBatches: toBatches,
    _parseMqttMessage: parseMqttMessage,
    _parseLegacyMqttMessage: parseLegacyMqttMessage,
    _processImageBundle: processImageBundle,
    processLegacyImageMessage: processLegacyImageMessage,
    processImageMessage: processImageMessage,
    processBacklog: processBacklog,
    clearBacklog: clearBacklog
}
